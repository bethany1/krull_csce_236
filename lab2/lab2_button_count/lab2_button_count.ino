/*--------------------------------------------------------------------
Name:   Bethany Krull
Date:   02/23/2021
Course: CSCE 236 Embedded Systems (Spring 2021) 
File:   lab2_button_count.ino
HW/Lab: Lab 2, Button Count Code

Purp: Uses timers and counters to detect button bounce and time
      button bounces/presses

Doc:  I talked with the professor on checkoff 5.

Academic Integrity Statement: I certify that, while others may have 
assisted me in brain storming, debugging and validating this program, 
the program itself is my own work. I understand that submitting code 
which is the work of other individuals is a violation of the honor   
code.  I also understand that if I knowingly give my original work to 
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

#include <Arduino.h>
//#include <avr/io.h>

// Definitions for Port B & D registors
#define PINB_Reg (*((volatile uint8_t *) 0x23))
#define DDRB_Reg (*((volatile uint8_t *) 0x24))
#define PORTB_Reg (*((volatile uint8_t *) 0x25))
#define PIND_Reg (*((volatile uint8_t *) 0x29))
#define DDRD_Reg (*((volatile uint8_t *) 0x2a))
#define PORTD_Reg (*((volatile uint8_t *) 0x2b))
// Definitions for LED assignments:
#define BOARD_LED 5   //pin 13 is PortB bit 5
#define RED_LED 1     //pin 9 is PortB bit 1  
#define GREEN_LED 2  //pin 10 is PortB bit 2
#define BLUE_LED 3   //pin 11 is PortB bit 3

#define BUTTON 8  //5 

//MY VARIABLES
uint16_t before = 0;
unsigned long bounceDelay = 0;
uint16_t prescalerTime = 4; //in microseconds
unsigned long prescalerTime2 = 0.004; //in milliseconds
uint8_t numPresses = 0;
unsigned long pressTime = 0;


/**
 * Init all of the LEDs and test them
 **/ 
void LEDInit(){
 //Set pinmode for LEDs to output 
  DDRB_Reg |= (1 << BOARD_LED);
  DDRB_Reg |= (1 << RED_LED);
  DDRB_Reg |= (1 << GREEN_LED);
  DDRB_Reg |= (1 << BLUE_LED);

  //Turn all off
  PORTB_Reg &= ~(1 << BOARD_LED); //clear output
  PORTB_Reg &= ~(1 << RED_LED);   //clear output
  PORTB_Reg &= ~(1 << GREEN_LED); //clear output
  PORTB_Reg &= ~(1 << BLUE_LED);  //clear output

  //Test LEDs
  Serial.println("Testing LEDs...");
  PORTB_Reg |= (1 << BOARD_LED);  //set output
  PORTB_Reg |= (1 << RED_LED);    //set output
  delay(400);
  PORTB_Reg &= ~(1 << RED_LED);   //clear output
  PORTB_Reg |= (1 << GREEN_LED);  //set output
  delay(400);
  PORTB_Reg &= ~(1 << GREEN_LED); //clear output
  PORTB_Reg |= (1 << BLUE_LED);   //set output
  delay(400);
  PORTB_Reg &= ~(1 << BLUE_LED);   //clear output
  PORTB_Reg &= ~(1 << BOARD_LED);   //clear output
  Serial.println("Finished LED testing!");
  }

//MY FUNCTIONS

void check1(){
  //compares TCNT1 to its last reading (before)
  if(TCNT1 > before){
    //detects bounce using comparison to before
    if((TCNT1 - before) > 1 ){
      Serial.println("ERROR - button bounce");
      TCNT1 = before + 1;
    }
    else{
     Serial.print("TCNT1: ");
     Serial.println(TCNT1);
     before = TCNT1;
    }
  }
  delay(100);
}

void check3(){
  //clear counts when button - pressed
  if(digitalRead(BUTTON) == 0){
    TCNT1 = 0;
    ICR1 = 0;
    delay(100);
    //detects bounce
    if(ICR1 != 0){
      bounceDelay = ICR1 * prescalerTime;
      Serial.println("Bounce Time in MicroSec: ");
      Serial.println(bounceDelay);
    }
    else{
      numPresses++;
      Serial.println("Number of Button Presses: ");
      Serial.println(numPresses);
    }
  }
}

void check4(){
  //button - pressed
  if(digitalRead(BUTTON) == 0){
    //button - released, clear counts
    if(digitalRead(BUTTON) == 1){
      TCNT1 = 0;
      ICR1 = 0;
      delay(100);
      //detects bounce
      if(ICR1 != 0){
        bounceDelay = ICR1 * prescalerTime;
        Serial.println("Bounce Time in MicroSec: ");
        Serial.println(bounceDelay);
      }
      else{
        numPresses++;
        Serial.println("Number of Button Presses: ");
        Serial.println(numPresses);
      }
    }
  }
}

void check5(){
  TCCR1B |= (1 <<ICES1);    //edge capture on rising
  while(1){
    //Hold while button - not pressed
    while(digitalRead(BUTTON) == 1);
    //clear counts once button - pressed
    TCNT1 = 0;
    ICR1 = 0;
    //hold while button - pressed
    while(digitalRead(BUTTON) == 0);
      //print time once button - released
      pressTime = (ICR1 * prescalerTime2);
      Serial.println("Time Pressed (mSec):");
      Serial.println(pressTime);
      Serial.println("");
  }
}


void setup() {                
  Serial.begin(9600);
  Serial.println("Starting up...");
  LEDInit();
  //Set pinmode for Button as input
  DDRD_Reg &= ~(1 << BUTTON);
  //Enable pullup 
  PORTD_Reg |= (1 << BUTTON);  //set output to enable pullup resistor

  
  //Init counter1
  TCCR1A = 0; //Normal mode 0xffff top, rolls over
  TCCR1B = (1 << CS11) | (1 << CS10); //prescalar - 64
  TCCR1C = 0;
  //Set counter to zero, high byte first
  TCNT1H = 0;
  TCNT1L = 0;  
  //Make sure interrupts are disabled 
  TIMSK1 = 0;
  TIFR1 = 0;
  
  Serial.println("Finished setup!");
}


void loop() {
  //check1();
  //check3();
  //check4();
  //check5();
}
