
void pullupReg(){
  #define BUTTON 2 //board pin 3 port c bit 2

  #define PINC (*((volatile uint8_t *) 0x26)
  #define DDRC (*((volatile uint8_t *) 0x27)
  #define PORTC (*((volatile uint8_t *) 0x28)
  #define MCUCR (*((volatile uint8_t *) 0x55)

  //enable pullup
  DDRC = 0;
  PORTC |= (1 << BUTTON);
  
  //mcucr = 1 then pullups are disabled
  //MCUCR |= (1 << 4);
}

void pullupArduino(){
  const uint8_t BUTTON = 3;
  pinMode(BUTTON, INPUT);
  digitalWrite(BUTTON, HIGH);
}

void setup() {
  //pullupReg();
  //pullupArduino;
}

void loop() {
  
}
