

void sub8Bit(){
  int8_t num1, num2;
  volatile int8_t result = 0;
  
  num1 = (rand() & 0xFF);
  num2 = (rand() & 0xFF);
  asm volatile("nop");
  result = num1 - num2;
  asm volatile("nop");
  Serial.println(result);
}

void sub16Bit() {
  int16_t num1, num2;
  volatile int16_t result = 0;
  
  num1 = (rand() & 0xFFFF);
  num2 = (rand() & 0xFFFF);
  asm volatile("nop");
  result = num1 - num2;
  asm volatile("nop");
  Serial.println(result);
}

void sub32Bit() {
  int32_t num1, num2;
  volatile int32_t result = 0;
  
  num1 = (rand() & 0xFFFFFFFF);
  num2 = (rand() & 0xFFFFFFFF);
  asm volatile("nop");
  result = num1 - num2;
  asm volatile("nop");
  Serial.println(result);
}

void div8Bit(){
  int8_t num1, num2;
  volatile int8_t result = 0;
  
  num1 = (rand() & 0xFF);
  num2 = (rand() & 0xFF);
  asm volatile("nop");
  result = num1 / num2;
  asm volatile("nop");
  Serial.println(num1);
  Serial.println(num2);
  Serial.println(result);
  Serial.println("...");
}

void div16Bit(){
  int16_t num1, num2;
  volatile int16_t result = 0;
  
  num1 = (rand() & 0xFFFF);
  num2 = (rand() & 0xFFFF);
  asm volatile("nop");
  result = num1 / num2;
  asm volatile("nop");
  Serial.println(result);
}

void div32Bit(){
  int32_t num1, num2;
  volatile int32_t result = 0;
  
  num1 = (rand() & 0xFFFFFFFF);
  num2 = (rand() & 0xFFFFFFFF);
  asm volatile("nop");
  result = num1 / num2;
  asm volatile("nop");
  Serial.println(result);
}

void subFloat(){
  float num1, num2;
  volatile float result = 0;
  long subTime = 0;
  long t1 = 0;
  long t2 = 0;
  
  num1 = (rand() & 0xFFFFFFFF);
  num2 = (rand() & 0xFFFFFFFF);
  asm volatile("nop");
  t1 = micros();
  result = num1 - num2;
  t2 = micros();
  asm volatile("nop");
  subTime = t2 - t1;
  Serial.println(result);
  Serial.println(subTime);
}

divFloat(){
  float num1, num2;
  volatile float result = 0;
  long subTime = 0;
  long t1 = 0;
  long t2 = 0;
  
  num1 = (rand() & 0xFFFFFFFF);
  num2 = (rand() & 0xFFFFFFFF);
  asm volatile("nop");
  t1 = micros();
  result = num1 / num2;
  t2 = micros();
  asm volatile("nop");
  subTime = t2 - t1;
  Serial.println(result);
  Serial.println(subTime);
}

void setup() {
 Serial.begin(9600);
 Serial.println("begin");
}

void loop() {
  //sub8Bit();
  //sub16Bit();
  //sub32Bit();
  //div8Bit();
  //div16Bit();
  //div32Bit();
  //subFloat();
  //divFloat();   
}
