# 1 "C:\\CODING\\csce236\\hmwk3\\timingOperations\\timingOperations.ino"

volatile int8_t result = 0;

void setup() {
 Serial.begin(9600);
 Serial.println("begin");
}

void loop() {
  int8_t num1, num2;

  num1 = (rand() & 0xFF);
  num2 = (rand() & 0xFF);
  asm volatile("nop");
  result = num1 - num2;
  asm volatile("nop");
  Serial.println(result);

}
