
#include "morse.h"

uint8_t led = 000;
unsigned long t_push = 0;
unsigned long t_release = 0;
unsigned long t_wait = 0;
unsigned long pushTime = 0;
unsigned long pauseTime = 0;
uint8_t morse = 0;
uint8_t buttonBit = 0;

void setupData(){
  #define redLED 1    //pin 9 portb bit 1
  #define greenLED 2  //pin 10 portb bit 2
  #define blueLED 3   //pin 11 portb bit 3
  #define BUTTON 4    //pin 12 portb bit 4

  #define PINB (*((volatile uint8_t *) 0x23))
  #define DDRB (*((volatile uint8_t *) 0x24))
  #define PORTB (*((volatile uint8_t *) 0x25))

  DDRB |= (1 << redLED);
  DDRB |= (1 << greenLED);
  DDRB |= (1 << blueLED);
  //button initialized as 0 - input

  //enable pullup resistor on BUTTON
  PORTB |= (1 << BUTTON);
}

void hi(){
  led = 010;  //green
  morseBlinkChar(led, 'h');

  led = 100;  //red
  morseBlinkChar(led, 'i');
}

//((PORTB >> BUTTON) & 1) == 1

int buttonMorse(){
  t_wait = millis();
  //not pushed
  while(!(PINB & (1 << BUTTON)));

  //pushed
  t_push = millis();
  pauseTime = t_push - t_wait;
  if(pauseTime > 2000){
    Serial.println("s");
    Serial.println("");
    return 3;
  }
  while(PINB & (1 << BUTTON));

  //not pushed 
  t_release = millis();
  pushTime = t_release - t_push;
  //Serial.println(pushTime);
  if(pushTime > 2000){
    Serial.println("D");
    Serial.println("");
    return 1;
  }
  else{
    Serial.println("d");
    Serial.println("");
    return 0;
  }  
}

void setLEDButton(){
  uint8_t n = 3;
  while((buttonBit = buttonMorse()) != 0b11){
    buttonBit &= 1;
    morse |= (buttonBit << n);
    n--;
  }

  if((morse >> 1) == 0b010){
    led = 100;
  }
  else if((morse >> 1) == 0b110){
    led = 010;
  }
  else if (morse == 0b1000){
    led = 001;
  }
  LED(led);
  delay(2000);
  allOff();
}

void setup() {
  Serial.begin(9600);
  Serial.println("begin ...");
  setupData();
  Serial.println("setup done!");
}

void loop() {
  hi();
  while(1);
}
