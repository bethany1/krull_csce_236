/*--------------------------------------------------------------------
Name:   Bethany Krull
Date:   03/26/21
Course: CSCE 236 Embedded Systems (Spring 2021)
File:   motors.cpp
HW/Lab: Project 1 and 2

Purp:   control robot motors to drive forwards/backward and turn based on time
        delays

Doc:  I worked on my own.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/
#include <avr/io.h>
#include "motors.h"
#include "Arduino.h"

//200
volatile uint8_t motorSpeed = 200;
//250
volatile uint8_t motorSpeedEXTRA = 250;
/*-------------------------------------------------------------------
      Defines Pin I/O
-------------------------------------------------------------------*/
void MOTOR_SETUP(){
  //define all motor control pins as arduino outputs
  DDRB |= (1 << leftEN);
  DDRD |= (1 << rightIN3);
  DDRD |= (1 << leftIN1) | (1 << leftIN2);
  DDRD |= (1 << rightEN);
  DDRD |= (1 << rightIN4);

  //SETUP TIMER2
    //set PWM - fast mode - non inverting mode
    TCCR2A |= (1 << COM2B1) | (1 << COM2A1) | (1 << WGM21) | (1 << WGM20);
}

/*
void setMotorSpeed(uint8_t inputSpeed){
  motorSpeed = inputSpeed;

  //Serial.println(motorSpeed);
}
*/

/*-----------------------------------------------------------------
      Drives forward for runTime in ms
-----------------------------------------------------------------*/
void forward(int16_t runTime){
  OCR2A = motorSpeed;
  OCR2B = motorSpeed+25;
  PORTD |= (1 << leftIN1);
  PORTD &= ~(1 << leftIN2);
  PORTD |= (1 << rightIN3);
  PORTD &= ~(1 << rightIN4);

  delay(runTime);
  OCR2A = 10;
  OCR2B = 10;
}

void forwardEXTRA(int16_t runTime){
  OCR2A = motorSpeed;
  OCR2B = motorSpeedEXTRA;
  PORTD |= (1 << leftIN1);
  PORTD &= ~(1 << leftIN2);
  PORTD |= (1 << rightIN3);
  PORTD &= ~(1 << rightIN4);

  delay(runTime);
  OCR2A = 10;
  OCR2B = 10;
}

/*-----------------------------------------------------------------
      Drives backward for runTime in ms
-----------------------------------------------------------------*/
void backward(uint16_t runTime){
  OCR2A = motorSpeed;
  OCR2B = motorSpeed;
  PORTD &= ~(1 << leftIN1);
  PORTD |= (1 << leftIN2);
  PORTD &= ~(1 << rightIN3);
  PORTD |= (1 << rightIN4);

  delay(runTime);
  OCR2A = 10;
  OCR2B = 10;
}

/*------------------------------------------------------------------
      Turns left on the spot (both motors active) for runTime in ms
------------------------------------------------------------------*/
void turnLeft(uint16_t runTime){
  OCR2A = motorSpeed;
  OCR2B = motorSpeed;
  PORTD &= ~(1 << leftIN1);
  PORTD |= (1 << leftIN2);
  PORTD |= (1 << rightIN3);
  PORTD &= ~(1 << rightIN4);

  delay(runTime);
  OCR2A = 10;
  OCR2B = 10;
}

/*-----------------------------------------------------------------
      Turns right on the spot (both motors active) for runTime in ms
-----------------------------------------------------------------*/
void turnRight(uint16_t runTime){
  OCR2A = motorSpeed;
  OCR2B = motorSpeed;
  PORTD |= (1 << leftIN1);
  PORTD &= ~(1 << leftIN2);
  PORTD &= ~(1 << rightIN3);
  PORTD |= (1 << rightIN4);

  delay(runTime);
  OCR2A = 10;
  OCR2B = 10;
}
