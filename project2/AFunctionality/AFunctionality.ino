/*--------------------------------------------------------------------
Name:   Bethany Krull
Date:   034/30/21
Course: CSCE 236 Embedded Systems (Spring 2021)
File:   AFunctionality.ino
HW/Lab: Project 2

Purp:   use motor library files and servo-sonic to navigate robot through
        obstacle course

Doc:  I worked on my own.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

#include <avr/io.h>
#include "motors.h"

  volatile uint16_t distLEFT = 0;
  volatile uint16_t distCENTER = 0;
  volatile uint16_t distRIGHT = 0;

void setup() {
  #define leftEN    3    //arduino pin 11 - atmega port B bit 3
  #define leftIN1   6    //arduino pin 6 - atmega port D bit 6
  #define leftIN2   5    //arduino pin 5 - atmega port D bit 5
  #define rightEN   3    //arduino pin 3 - atmega port D bit 3
  #define rightIN3  1    //arduino pin 1 - atmega port D bit 1
  #define rightIN4  4    //arduino pin 4 - atmega port D bit 4

  #define servo 2           //arduino pin 10 - port B bit 2
  #define sonicTrigger 7    //arduino pin 7 - port D bit 7
  #define sonicEcho 0       //arduino pin 8 - port B bit 0

  #define servoLEFT  1
  #define servoCENTER 2
  #define servoRIGHT 3

  //All I/O:
  //motors
  DDRB |= (1 << leftEN);
  DDRD |= (1 << leftIN1) | (1 << leftIN2);
  DDRD |= (1 << rightEN);
  DDRD |= (1 << rightIN3) | (1 << rightIN4);
  //define SERVO as output
  DDRB |= (1 << servo);
  DDRB |= (1 << 1);
  //define SONIC I/O
  DDRB &= ~(1 << sonicEcho);
  DDRD |= (1 << sonicTrigger);

  //setup TIMER2 (motor):
    //set PWM - fast mode - non inverting mode
    TCCR2A |= (1 << COM2B1) | (1 << COM2A1) | (1 << WGM21) | (1 << WGM20);

  //setup TIMER1 (servoSonic):
    //PWM mode 11: fast mode/top=OCR1A
    TCCR1A |= (1 << WGM11) | (1 << WGM10);
    TCCR1B |= (1 << WGM13) | (1 << WGM12);

    //control OCR - for phase correct
    //clear OC1B on comp match when up-counting ... clear when down-counting
    TCCR1A |= (1 << COM1B1);
    TCCR1A |= (1 << COM1A1);
    //prescaler 64
    //CS12 doesn't work right - 64 is MAX
    TCCR1B |= (1 << CS11) | (1 << CS10);

    //set Output Compare Registers (servo turns)
    OCR1A = 5000;
    OCR1B = 145;
    //OCR1B values:
    //375 - center
    //250 - right 45
    //145 - right 90
    //450 - left 45
    //600 - left 90

    TCCR1C = 0;
}

uint16_t readDist(){
  //trigger the ultraSonic
  TCNT1 = 0;
  ICR1 = 0;
  PORTD |= (1 << sonicTrigger);
  delayMicroseconds(10);
  PORTD &= ~(1 << sonicTrigger);

  //wait till ICR1 recieves the echo signal
  while(ICR1 == 0){}
  //calculates distance using ICR1 counts and speed of sound
  uint16_t echoCount = ICR1;
  uint16_t echoTime = echoCount * 4 / 2; //in microseconds (half)
  uint16_t echoDist = echoTime * 0.0135;
  //1 foot is between 25-27
  delay(60);
  return echoDist;
}

void servoMove(uint8_t position){
  if(position == servoRIGHT){
    OCR1B = 145;
    delay(125);
    distRIGHT = readDist();
  }
  else if(position == servoCENTER){
    OCR1B = 375;
    delay(125);
    distCENTER = readDist();
  }
}

void loop() {
  servoMove(servoCENTER);
  servoMove(servoRIGHT);

  if(distCENTER < 23){
    turnLeft(185);
  }
  //adjust straight
  //if bot too far from right wall - close to wall
  else if(distRIGHT > 25 && distRIGHT <= 26){
    turnRight(25);
    forward(110);
   }
  //if bot too far from wall - not close to wall/in the open
  else if(distRIGHT > 25){
    turnRight(12);
    forward(110);
  }
  //if bot too close to right wall
  else if(distRIGHT < 25){
    turnLeft(25);
    forward(110);
    }
  //if bot alignment is correct
  else{
    forward(150);
  }
}
