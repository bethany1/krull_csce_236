/*--------------------------------------------------------------------
Name:   Bethany Krull
Date:   03/10/21
Course: CSCE 236 Embedded Systems (Spring 2021)
File:   servoSonic.h
HW/Lab: LAB 3

Purp:   Assemble the robot kit and get familiar with both the servo
        and the ultrasonic.

Doc:  I talked with the professor and Patrick about some board setup
      and servo setup.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/
#ifndef SERVO_SONIC_H
#define SERVO_SONIC_H

#include <avr/io.h>



/*---------------------------------------------------------------
			Pin/Port set up
----------------------------------------------------------------*/
  #define servo 3           //arduino pin 3 - port d bit 3
  #define leftLED 6         //arduino pin 6 - port d bit 6
  #define rightLED 5        //arduino pin 5 - port d bit 5
  #define sonicTrigger 7    //arduino pin 7 - port d bit 7
  #define sonicEcho 0       //arduino pin 8 - port b bit 0

  #define servoLEFT  1
  #define servoCENTER 2
  #define servoRIGHT 3

/*---------------------------------------------------------------
	Function prototypes for A Functionality
---------------------------------------------------------------*/
void SERVO_SONIC_SETUP();

//moves servo -90 ~ 0 ~ 90 degrees
void servoMove(uint8_t);

//measures/returns distance from sonic to wall
uint8_t sonicDist();

//detects wall to left/center/right 
void sonicObjectDetect();

#endif
