/*--------------------------------------------------------------------
Name:   Bethany Krull
Date:   03/10/21
Course: CSCE 236 Embedded Systems (Spring 2021)
File:   servoSonic.cpp
HW/Lab: LAB 3

Purp:   Assemble the robot kit and get familiar with both the servo
        and the ultrasonic.

Doc:  I talked with the professor and Patrick about some board setup
      and servo setup.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/
#include <avr/io.h>
#include "servoSonic.h"
#include "Arduino.h"



/*------------------------------------------------------------------
		sets up timers, I/O, serial port
-------------------------------------------------------------------*/
void SERVO_SONIC_SETUP() {
//setup IO
  DDRB |= (1 << servo);
  DDRB |= (1 << 1);
  //define SONIC I/O
  DDRB &= ~(1 << sonicEcho);
  DDRD |= (1 << sonicTrigger);

//setup TIMER1 (servoSonic):
  //PWM mode 11: fast mode/top=OCR1A 
  TCCR1A |= (1 << WGM11) | (1 << WGM10); 
  TCCR1B |= (1 << WGM13) | (1 << WGM12);

  //control OCR - for phase correct
  //clear OC1B on comp match when up-counting ... clear when down-counting
  TCCR1A |= (1 << COM1B1);
  TCCR1A |= (1 << COM1A1);
  //prescaler 64
  //CS12 doesn't work right - 64 is MAX
  TCCR1B |= (1 << CS11) | (1 << CS10);
  

  //set Output Compare Registers (servo turns)
  OCR1A = 5000;
  OCR1B = 145;
  //OCR1B values:
  //375 - center
  //250 - right 45
  //145 - right 90
  //450 - left 45
  //600 - left 90
  
  TCCR1C = 0;
  delay(15000);
}


/*------------------------------------------------------------------------
	moves servo left/right/center depending on passed parameter	
-------------------------------------------------------------------------*/
void servoMove(uint8_t position){
  if(position == servoLEFT){
    OCR2B = 16;
    delay(1000);
  }
  else if(position == servoRIGHT){
    OCR2B = 3;
    delay(1000);
  }
  else if(position == servoCENTER){
    OCR2B = 8; 
    delay(1000);
  }
}


/*---------------------------------------------------------------------------
	calculates distance between robot and wall using ultra-sonic sensor
----------------------------------------------------------------------------*/
uint8_t sonicDist(){
  TCNT1 = 0;
  ICR1 = 0;
  //trigger 10 microsec
  PORTD |= (1 << sonicTrigger);
  delayMicroseconds(10); 
  PORTD &= ~(1 << sonicTrigger);

  while(ICR1 == 0){}
  uint16_t echoCount = (ICR1);           
  float distCm = echoCount * 0.010625;      //calculated distance in inches  
  //(time per cy (microsec) * speed of sound (cm/microsec) * 0.5)
  //(0.0625 * 0.034 * 0.5)

  delay(60);
  return distCm;
}


/*-------------------------------------------------------------------------------
	detects walls to left/right/center of robot and indicates walls via LEDs
-------------------------------------------------------------------------------*/

/*
void sonicObjectDetect(){
  servoMove(servoLEFT);
  delay(1000);
  uint8_t distLeft = sonicDist();

  servoMove(servoCENTER);
  delay(1000);
  uint8_t distCenter = sonicDist();

  servoMove(servoRIGHT);
  delay(1000);
  uint8_t distRight = sonicDist();

/*
  //sets appropriate LED depending on obstacle 
  //number adjusted to account for inaccurate dist math
  if(distLeft <= 10){        
    PORTD |= (1 << leftLED);
  }
  if(distRight <= 10){
    PORTD |= (1 << rightLED);
  }
  if(distCenter <= 10){
    PORTD |= (1 << leftLED) | (1 << rightLED);
  }
/*
}
*/
