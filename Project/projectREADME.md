Project/combo contains the code for the final wall-following functionality of the project. 
Project/motorController is the code for the robot dancing functionality.
Project/servoSonicInt is the code from lab3 copied over and modified to fit an updated timer
set-up.
Project/motorLibray contains the h and cpp files for driving the robot.