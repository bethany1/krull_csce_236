void setup() {
  #define leftEN    3    //arduino pin 11 - atmega port B bit 3
  #define leftIN1   6    //arduino pin 6 - atmega port D bit 6
  #define leftIN2   5    //arduino pin 5 - atmega port D bit 5
  #define rightEN   3    //arduino pin 3 - atmega port D bit 3
  #define rightIN3  2    //arduino pin 2 - atmega port D bit 2
  #define rightIN4  4    //arduino pin 4 - atmega port D bit 4
  
  //define all motor control pins as arduino outputs
  DDRB |= (1 << leftEN);
  DDRD |= (1 << leftIN1) | (1 << leftIN2);
  DDRD |= (1 << rightEN);
  DDRD |= (1 << rightIN3) | (1 << rightIN4);

  //SETUP TIMER2
    //set PWM - fast mode - non inverting mode
    TCCR2A |= (1 << COM2B1) | (1 << COM2A1) | (1 << WGM21) | (1 << WGM20);
  
    Serial.begin(9600);
    Serial.println("begin ....");
}

void forward(int16_t runTime){
  OCR2A = 200;
  OCR2B = 200;
  PORTD |= (1 << leftIN1);
  PORTD &= ~(1 << leftIN2);
  PORTD |= (1 << rightIN3);
  PORTD &= ~(1 << rightIN4);

  delay(runTime);
  OCR2A = 10;
  OCR2B = 10;
}

void backward(uint16_t runTime){
  OCR2A = 200;
  OCR2B = 200;
  PORTD &= ~(1 << leftIN1);
  PORTD |= (1 << leftIN2);
  PORTD &= ~(1 << rightIN3);
  PORTD |= (1 << rightIN4);

  delay(runTime);
  OCR2A = 10;
  OCR2B = 10;
}

void turnLeft(uint16_t runTime){
  OCR2A = 200;
  OCR2B = 200;
  PORTD &= ~(1 << leftIN1);
  PORTD |= (1 << leftIN2);
  PORTD |= (1 << rightIN3);
  PORTD &= ~(1 << rightIN4);

  delay(runTime);
  OCR2A = 10;
  OCR2B = 10;
}

void turnRight(uint16_t runTime){
  OCR2A = 200;
  OCR2B = 200;
  PORTD |= (1 << leftIN1);
  PORTD &= ~(1 << leftIN2);
  PORTD &= ~(1 << rightIN3);
  PORTD |= (1 << rightIN4);

  delay(runTime);
  OCR2A = 10;
  OCR2B = 10;
}

void loop() {
  forward(300);
  delay(1000);

  backward(300);
  delay(1000);
 
  turnLeft(75);
  delay(1000);
  turnRight(75);
  delay(1000);
  turnRight(75);
  delay(1000);
  turnLeft(75);
  delay(1000);

  turnLeft(150);
  delay(1000);
  turnRight(150);
  delay(1000);
  turnRight(150);
  delay(1000);
  turnLeft(150);  

  delay(10000);
}
