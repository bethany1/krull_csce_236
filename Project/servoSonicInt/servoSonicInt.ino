/*--------------------------------------------------------------------
Name:   Bethany Krull
Date:   03/10/21
Course: CSCE 236 Embedded Systems (Spring 2021)
File:   servoSonic.ino
HW/Lab: LAB 3

Purp:   Assemble the robot kit and get familiar with both the servo
        and the ultrasonic.

Doc:  I talked with the professor and Patrick about some board setup
      and servo setup.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

void setup() {
  #define servo 2           //arduino pin 10 - port B bit 2
  #define leftLED 4         //arduino pin 4 - port D bit 4
  #define rightLED 0        //arduino pin 0 - port D bit 0
  #define sonicTrigger 7    //arduino pin 7 - port D bit 7
  #define sonicEcho 0       //arduino pin 8 - port B bit 0
  #define LEFT  1
  #define CENTER 2
  #define RIGHT 3

  Serial.begin(9600);
  Serial.println("begin....");

  //setup SERVO as output
  DDRB |= (1 << servo);

  //setup SONIC I/O
  DDRB &= ~(1 << sonicEcho);
  DDRD |= (1 << sonicTrigger);

  //set up LEDS as output
  DDRD |= (1 << leftLED);
  DDRD |= (1 << rightLED);

//SET UP TIMER1:
  //PWM mode 11: phase correct/top=OCR1A 
  TCCR1A |= (1 << WGM11) | (1 << WGM10); 
  TCCR1B |= (1 << WGM13) | (1 << WGM12);

  //control OCR - for phase correct
  //clear OC1A/B on comp match when up-counting ... clear when down-counting
  TCCR1A |= (1 << COM1A1) | (1 << COM1B1);

  //prescaler 8
  TCCR1B |= (1 << CS11);

  //set Output Compare Registers (servo turns)
  OCR1A = 20000;
  OCR1B = 1000;   //left

  //-  detect falling edge
  TCCR1A = 0;   //normal mode
  TCCR1C = 0;
  //Set counter to zero, high byte first
  TCNT1H = 0;
  TCNT1L = 0;  
  //Make sure interrupts are disabled 
  TIMSK1 = 0;
  TIFR1 = 0;




}

void servoMove(uint8_t position){
  Serial.println("in move");
  if(position == LEFT){
    Serial.println("in left");
    OCR1B = 1000;
    delay(1000);
  }
  else if(position == RIGHT){
    OCR1B = 3000;
    delay(1000);
  }
  else if(position == CENTER){
    OCR1B = 2000; 
    delay(1000);
  }
}

uint8_t sonicDist(){
  TCNT1 = 0;
  ICR1 = 0;
  //trigger 10 microsec
  PORTD |= (1 << sonicTrigger);
  delayMicroseconds(10); 
  PORTD &= ~(1 << sonicTrigger);

  while(ICR1 == 0){}
  uint16_t echoCount = (ICR1);           
  float distCm = echoCount * 0.010625;      //calculated distance in inches  
  //(time per cy (microsec) * speed of sound (cm/microsec) * 0.5)
  //(0.0625 * 0.034 * 0.5)

  delay(60);
  return distCm;
}

void sonicObjectDetect(){
  servoMove(LEFT);
  delay(1000);
  uint8_t distLeft = sonicDist();

  servoMove(CENTER);
  delay(1000);
  uint8_t distCenter = sonicDist();

  servoMove(RIGHT);
  delay(1000);
  uint8_t distRight = sonicDist();

  //sets appropriate LED depending on obstacle 
  //number adjusted to account for inaccurate dist math
  if(distLeft <= 10){        
    PORTD |= (1 << leftLED);
  }
  if(distRight <= 10){
    PORTD |= (1 << rightLED);
  }
  if(distCenter <= 10){
    PORTD |= (1 << leftLED) | (1 << rightLED);
  }
}

void loop() {
  int8_t position = LEFT;
  
  if(position == LEFT){
    Serial.println("in left");
    OCR1B = 1000;
    delay(1000);
  }
  else if(position == RIGHT){
    OCR1B = 3000;
    delay(1000);
  }
  else if(position == CENTER){
    OCR1B = 2000; 
    delay(1000);
  }
  while(1){}
}
