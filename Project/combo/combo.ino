void setup() {
  #define leftEN    3    //arduino pin 11 - atmega port B bit 3
  #define leftIN1   6    //arduino pin 6 - atmega port D bit 6
  #define leftIN2   5    //arduino pin 5 - atmega port D bit 5
  #define rightEN   3    //arduino pin 3 - atmega port D bit 3
  #define rightIN3  2    //arduino pin 2 - atmega port D bit 2
  #define rightIN4  4    //arduino pin 4 - atmega port D bit 4

  #define servo 2           //arduino pin 10 - port B bit 2
  #define sonicTrigger 7    //arduino pin 7 - port D bit 7
  #define sonicEcho 0       //arduino pin 8 - port B bit 0
  #define LEFT  1
  #define CENTER 2
  #define RIGHT 3

  
  //All I/O:
  //motors
  DDRB |= (1 << leftEN);
  DDRD |= (1 << leftIN1) | (1 << leftIN2);
  DDRD |= (1 << rightEN);
  DDRD |= (1 << rightIN3) | (1 << rightIN4);
  //define SERVO as output
  DDRB |= (1 << servo);
  DDRB |= (1 << 1);
  //define SONIC I/O
  DDRB &= ~(1 << sonicEcho);
  DDRD |= (1 << sonicTrigger);

  //setup TIMER2 (motor):
    //set PWM - fast mode - non inverting mode
    TCCR2A |= (1 << COM2B1) | (1 << COM2A1) | (1 << WGM21) | (1 << WGM20);

  //setup TIMER1 (servoSonic):
    //PWM mode 11: fast mode/top=OCR1A 
    TCCR1A |= (1 << WGM11) | (1 << WGM10); 
    TCCR1B |= (1 << WGM13) | (1 << WGM12);

    //control OCR - for phase correct
    //clear OC1B on comp match when up-counting ... clear when down-counting
    TCCR1A |= (1 << COM1B1);
    TCCR1A |= (1 << COM1A1);
    //prescaler 64
    //CS12 doesn't work right - 64 is MAX
    TCCR1B |= (1 << CS11) | (1 << CS10);
    

    //set Output Compare Registers (servo turns)
    OCR1A = 5000;
    OCR1B = 145;
    //OCR1B values:
    //375 - center
    //250 - right 45
    //145 - right 90
    //450 - left 45
    //600 - left 90
    
    TCCR1C = 0;
  delay(15000);
}

uint16_t readDist(){
  //trigger the ultraSonic
  TCNT1 = 0;
  ICR1 = 0;
  PORTD |= (1 << sonicTrigger);
  delayMicroseconds(10);
  PORTD &= ~(1 << sonicTrigger);

  //wait till ICR1 recieves the echo signal
  while(ICR1 == 0){}
  //calculates distance using ICR1 counts and speed of sound
  uint16_t echoCount = ICR1;
  uint16_t echoTime = echoCount * 4 / 2; //in microseconds (half)
  uint16_t echoDist = echoTime * 0.0135;
  //1 foot is between 25-27
  delay(60);
  return echoDist;
}

void forward(int16_t runTime){
  OCR2A = 200;
  OCR2B = 200;
  PORTD |= (1 << leftIN1);
  PORTD &= ~(1 << leftIN2);
  PORTD |= (1 << rightIN3);
  PORTD &= ~(1 << rightIN4);

  delay(runTime);
  OCR2A = 10;
  OCR2B = 10;
}

void backward(uint16_t runTime){
  OCR2A = 200;
  OCR2B = 200;
  PORTD &= ~(1 << leftIN1);
  PORTD |= (1 << leftIN2);
  PORTD &= ~(1 << rightIN3);
  PORTD |= (1 << rightIN4);

  delay(runTime);
  OCR2A = 10;
  OCR2B = 10;
}

void turnLeft(uint16_t runTime){
  OCR2A = 10;
  OCR2B = 200;
  PORTD &= ~(1 << leftIN1);
  PORTD |= (1 << leftIN2);
  PORTD |= (1 << rightIN3);
  PORTD &= ~(1 << rightIN4);

  delay(runTime);
  OCR2A = 10;
  OCR2B = 10;
}

void turnRight(uint16_t runTime){
  OCR2A = 200;
  OCR2B = 10;
  PORTD |= (1 << leftIN1);
  PORTD &= ~(1 << leftIN2);
  PORTD &= ~(1 << rightIN3);
  PORTD |= (1 << rightIN4);

  delay(runTime);
  OCR2A = 10;
  OCR2B = 10;
}

void adjustDrive(uint8_t direction){
  if(direction == LEFT){
    turnLeft(25);
  }
  else{
    turnRight(25);
  }
}

void loop() {
  uint16_t distFromWall = 0;

  distFromWall = readDist();
  if(distFromWall > 26){
    turnRight(25)
    forward(50);
  }
  else if(distFromWall < 26){
    turnLeft(25);
    forward(25);
  }
  else{
    forward(50);
  }
  
  delay(55);
}
