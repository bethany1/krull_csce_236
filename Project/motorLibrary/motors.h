/*--------------------------------------------------------------------
Name:   Bethany Krull
Date:   03/26/21
Course: CSCE 236 Embedded Systems (Spring 2021)
File:   motors.h
HW/Lab: Project 1

Purp:   control robot motors to drive forwards/backward and turn based on time
        delays

Doc:  I worked on my own.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/
#ifndef MOTORS_H
#define MOTORS_H

#include <avr.io.h>

/*--------------------------------------------------------------------
      Pin/Port Set Up
--------------------------------------------------------------------*/
#define leftEN    3    //arduino pin 11 - atmega port B bit 3
#define leftIN1   6    //arduino pin 6 - atmega port D bit 6
#define leftIN2   5    //arduino pin 5 - atmega port D bit 5
#define rightEN   3    //arduino pin 3 - atmega port D bit 3
#define rightIN3  2    //arduino pin 2 - atmega port D bit 2
#define rightIN4  4    //arduino pin 4 - atmega port D bit 4

/*-------------------------------------------------------------------
      Function Prototypes for Motor Functionality
-------------------------------------------------------------------*/
//defines pin I/O
void setup();

//drives forward for x ms
void forward(int16_t);

//drives backwards for x ms
void backward(uint16_t);

//turns left on the spot (both motors active) for x ms
void turnLeft(uint16_t);

//turns right on the spot (both motors active) for x ms
void turnRight(uint16_t);
