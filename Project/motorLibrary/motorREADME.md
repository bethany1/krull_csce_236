/*--------------------------------------------------------------------
Name:   Bethany Krull
Date:   03/26/21
Course: CSCE 236 Embedded Systems (Spring 2021)
File:   motorREADME.md
HW/Lab: Project 1

Purp:   explains how to use motor functions in motors.h/.cpp

Doc:  I worked on my own.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

The motor are design with 3 wires each. Each motor has an enable pin that is
controlled by a PWM signal created by timer2. The PWM is set-up in fast mode
(non-inverting) so the width of the pulse signal can be controlled by modifying
OCR1A/B registers. The higher the OCR1A/B values (range 0-255) the faster the
motor will run. A OCR1A/B value of 10 is low enough to effectively equal "zero"
pulse signal - 10 is used to disable the motors without technically stopping
the PWM signal. Here, I don't have speed variation set-up so OCR1A/B equals
200 is effectively "on".

The distance the motors run in all functions defined below are time-based. So,
the function intakes a user specified time in ms for the motors to run using a
delay() function. Once the specified time has passed both motors are disabled
with a forced stop awaiting further instructions.

All the turns I have here are "on the spot" turns because both motors are active
in turning the robot - 1 in forward() and 1 in backward(). For example, to turn
left the right motor pushes the robot forward and the left motor pulls it
backwards to turn it. 
