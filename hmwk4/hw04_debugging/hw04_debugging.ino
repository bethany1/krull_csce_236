/*
 * CSCE236 Embedded Systems Spring
 * HW4
 */

void debugLED(){
  #define red 4 //arduino pin 4
  pinMode(red, OUTPUT); 
}

//These are the functions that we know exist in obfs.cpp
void startOne();
void startTwo();
void startThree();
void startFour();
void setMem(char i);
void runLoop();


void setup() {                
  Serial.begin(9600);
  Serial.println("Starting up...");
  //
  digitalWrite(red, HIGH);
  delay(100);
  digitalWrite(red, LOW);
  delay(100);
  //
  digitalWrite(red, HIGH);
  delay(100);
  digitalWrite(red, LOW);
  delay(1500);

  //This order has been fixed
  
  startThree();
  startTwo();
  startFour();
  startOne();
  
Serial.begin(9600);
Serial.println("restart");

  //Write _YOUR_ initials to memory and determine where they are put
  //Note you should only do one at a time since they may overwrite each other
  
  #define arrayStart (*((volatile uint8_t*)0x40000814))
  //uint32_t *arrayStart = 0x40000800;
  setMem('b');

  for(int i = 0; i < 300; i++){
    uint32_t prtStmt = *(&arrayStart + 0);
    //uint32_t prtStmt = *(0x4000808);
    //uint32_t prtStmt = *(&arrayStart);
  
    Serial.println(prtStmt);
  }
  uint32_t prtStmt = &(*(&arrayStart + 8));
  //Serial.println(prtStmt);
  //setMem('d');



 //The serial port gets messed up, so this won't print
 Serial.println("Finished setup");

}

void loop() {
  //Figure out how long this function takes to run with
  //and without the button pressed.
  #define START 6   //arduino pin 6
  #define END 7     //arduino pin 7
  
  pinMode(START, OUTPUT);
  pinMode(END, OUTPUT);

  digitalWrite(START, HIGH);
  runLoop();
  digitalWrite(END, HIGH);

  delay(2000);

  digitalWrite(START, LOW);
  digitalWrite(END, LOW);
}
