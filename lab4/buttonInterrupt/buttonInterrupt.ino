void setup() {
  #define BUTTON 3  //arduino pin 3 - port d bit 3
  #define LED 0     //arduino pin 8 - port b bit 0

  //set I/O
  DDRD &= ~(1 << BUTTON);
  DDRB |= (1 << LED);

  //enable interrupt:
  //falling edge of INT1 generates interrupt request
  EICRA |= (1 << ISC11);
  //enable INT1
  EIMSK |= (1 << INT1);
}

ISR(INT1_vect){
   PORTB |= (1 << LED);
}

void loop() {
/*  
  //when BUTTON=LOW (pushed) turn on LED
  while(!(PIND & (1 << BUTTON))){
    PORTB |= (1 << LED);
  }
*/
  if(PIND & (1 << BUTTON)){
    cli();
    PORTB &= ~(1 << LED);
    delay(200);
    sei();
  }  

}
