/*--------------------------------------------------------------------
Name:   Bethany Krull
Date:   04/01/21
Course: CSCE 236 Embedded Systems (Spring 2021)
File:   buttonInterruptApproach1.ino
HW/Lab: LAB 4

Purp:   fix LED button toggle with approach 1 (enable/disable interrupt)

Doc:  I worked on my own.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/
void setup() {
  #define BUTTON 3  //arduino pin 3 - port d bit 3
  #define LED 0     //arduino pin 8 - port b bit 0

  //set I/O
  DDRD &= ~(1 << BUTTON);
  DDRB |= (1 << LED);

  //enable interrupt:
  //falling edge of INT1 generates interrupt request
  EICRA |= (1 << ISC11);
  //enable INT1
  EIMSK |= (1 << INT1);
}

ISR(INT1_vect){
  //LED on
   PORTB |= (1 << LED);
}

void loop() {
  //when button not pushed
  if(PIND & (1 << BUTTON)){
    cli();
    //LED off
    PORTB &= ~(1 << LED);
    delay(200);
    sei();
  }  

}
