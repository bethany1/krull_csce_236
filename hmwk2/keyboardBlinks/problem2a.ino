uint8_t keyboard = 0;
uint8_t pause = 500;

void setup() {
  Serial.begin(9600);
  while(!Serial){
    ;
  }
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  if(Serial.available > 0){
    keyboard = Serial.read(); 

    if(keyboard == 's'){
      pause = 1000;
    }
    else if(keyboard == 'f'){
      pause = 250;
    }
    
    for(int i = keyboard; i>0; i--){
      digitalWrite(LED_BUILTIN, HIGH);
      delay(pause);
      digitalWrite(LED_BUILTIN,LOW);
      delay(pause);
    }
  }

}
