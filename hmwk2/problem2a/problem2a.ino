/*--------------------------------------------------------------------
Name:   Bethany Krull
Date:   02/16/2021
Course: CSCE 236 Embedded Systems (Spring 2021)
File:   problem2a.ino
HW/Lab: Hmwk 2

Purp: Use the Serial Monitor to effect an LED. Blinks fast/slow and anywhere 
      between 1-9 times depending on keyboard inputs.

Doc:  I worked on my own.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/
char keyboard = 0;
uint16_t pause = 500;

void setup() {
  Serial.begin(9600);
  delay(2000);
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.print("begin");
}

void loop() {
  //IF SOMETHING HAS BEEN TYPED INTO SERIAL MONITOR
  if(Serial.available()){
    keyboard = Serial.read(); 

    //SET BLINK SPEED 
    if(keyboard == 's'){
      pause = 1000;
    }
    else if(keyboard == 'f'){
      pause = 250;
    }

    //BLINKS LED ACCORDING TO INPUT NUMBER
    if(keyboard != 's' && keyboard != 'f'){
      for(int i = ((int)keyboard - 48); i>0; i--){
        digitalWrite(LED_BUILTIN, HIGH);
        delay(pause);
        digitalWrite(LED_BUILTIN,LOW);
        delay(pause);
      }
    }
    keyboard = 0;
  }
}
