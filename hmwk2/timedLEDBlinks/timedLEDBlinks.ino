/*--------------------------------------------------------------------
Name:   Bethany Krull
Date:   02/17/21
Course: CSCE 236 Embedded Systems (Spring 2021)
File:   timedLEDBlinks.ino
HW/Lab: Homework2

Purp:   Use 8/16/32 bit loops to time an LED sequence.

Doc:  I worked on my own.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/
uint8_t count8 = 0xff;
uint16_t count16 = 0xffff;
uint32_t count32 = 0xffffffff;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  //6 SECONDS ON
  digitalWrite(LED_BUILTIN, HIGH);
  for(count32 = 0; count32 < 2700000; count32++){
    asm volatile("nop");
  }
  digitalWrite(LED_BUILTIN, LOW);
  for(count8 = 0; count8 < 255; count8++){
    for(uint8_t i = 0; i < 255; i++){
      for(uint8_t j = 0; j < 25; j++){
        asm volatile("nop");
      }
    }
  }
  
  //BLINK ONCE
  digitalWrite(LED_BUILTIN, HIGH);
  for(count8 = 0; count8 < 255; count8++){
    for(uint8_t i = 0; i < 255; i++){
      for(uint8_t j = 0; j < 25; j++){
        asm volatile("nop");
      }
    }
  }
  digitalWrite(LED_BUILTIN, LOW);
  for(count8 = 0; count8 < 255; count8++){
    for(uint8_t i = 0; i < 255; i++){
      for(uint8_t j = 0; j < 25; j++){
        asm volatile("nop");
      }
    }
  }

  //3 SECONDS ON
  digitalWrite(LED_BUILTIN, HIGH);
  for(count16 = 0; count16 < 10000; count16++){
    for(uint16_t i = 0; i < 850; i++){
      asm volatile("nop");
    }
  }
  digitalWrite(LED_BUILTIN, LOW);
  for(count8 = 0; count8 < 255; count8++){
    for(uint8_t i = 0; i < 255; i++){
      for(uint8_t j = 0; j < 25; j++){
        asm volatile("nop");
      }
    }
  }

  //BLINK TWICE
  digitalWrite(LED_BUILTIN, HIGH);
  for(count8 = 0; count8 < 255; count8++){
    for(uint8_t i = 0; i < 255; i++){
      for(uint8_t j = 0; j < 25; j++){
        asm volatile("nop");
      }
    }
  }
  digitalWrite(LED_BUILTIN, LOW);
  for(count8 = 0; count8 < 255; count8++){
    for(uint8_t i = 0; i < 255; i++){
      for(uint8_t j = 0; j < 25; j++){
        asm volatile("nop");
      }
    }
  }
  digitalWrite(LED_BUILTIN, HIGH);
  for(count8 = 0; count8 < 255; count8++){
    for(uint8_t i = 0; i < 255; i++){
      for(uint8_t j = 0; j < 25; j++){
        asm volatile("nop");
      }
    }
  }
  digitalWrite(LED_BUILTIN, LOW);
  for(count8 = 0; count8 < 255; count8++){
    for(uint8_t i = 0; i < 255; i++){
      for(uint8_t j = 0; j < 25; j++){
        asm volatile("nop");
      }
    }
  }

  //2 SECONDS ON
digitalWrite(LED_BUILTIN, HIGH);
  for(count8 = 0; count8 < 255; count8++){
    for(uint8_t i = 0; i < 255; i++){
      for(uint8_t j = 0; j < 150; j++){
        asm volatile("nop");
      }
    }
  }
  digitalWrite(LED_BUILTIN, LOW);
  
  while(1){}
}
