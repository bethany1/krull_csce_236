uint8_t count8 = 0xff;
uint16_t count16 = 0xffff;
uint32_t count32 = 0xffffffff;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {

  //8 BIT*****
  digitalWrite(LED_BUILTIN, HIGH);
  for(count8 = 0; count8 < 255; count8++){
    for(uint8_t i = 0; i < 255; i++){
        for(uint8_t j = 0; j < 255; j++){
            for(uint8_t k = 0; k < 7; k++){
              asm volatile("nop");
            }
        }
    }
  }
  digitalWrite(LED_BUILTIN, LOW);
  while(1){}

/*
  //16 BIT *****
  digitalWrite(LED_BUILTIN, HIGH);
  for(count16 = 0; count16 < 10000; count16++){
    //asm volatile("nop");
    for(uint16_t i = 0; i < 3500; i++){
      asm volatile("nop");
    }
  }
  digitalWrite(LED_BUILTIN, LOW);
  while(1){}
*/

/*
  //32 BIT*****
  digitalWrite(LED_BUILTIN, HIGH);
  for(count32 = 0; count32 < 20000000; count32++){
    asm volatile("nop"::);
  }
  digitalWrite(LED_BUILTIN, LOW);
  while(1){}
 */
}
