/*--------------------------------------------------------------------
Name:   Bethany Krull
Date:   02/09/21
Course: CSCE 236 Embedded Systems (Spring 2021)
File:   servo.ino
HW/Lab: Lab 1

Purp:   Move a servo motor through a pattern once a button is pushed
        -90, 0, 90, 0
        
Doc:    I worked on my own.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/
#include <Servo.h>

Servo myServo;
const uint8_t servoPin = 11;
const uint8_t buttonPin = 2;
uint8_t buttonState = 0;

void setup() {
  myServo.attach(servoPin);
  pinMode(buttonPin, INPUT);
}

void loop() {
  buttonState = digitalRead(buttonPin);
  
  //WHEN BUTTON IS PRESSED - SERVO ANGLE CYCLE
  if(buttonState == LOW){
    //SERVO RANGE:0-180
    //"-90" = 180 ON SERVO
    myServo.write(180);
    delay(1000);
    //"0" = 90 ON SERVO
    myServo.write(90);
    delay(1000);
    //"90" = 0 ON SERVO
    myServo.write(0);
    delay(1000);
    myServo.write(90);
    delay(1000);
  }
  //WHEN BUTTON NOT PRESSED - STAY AT "0"
  else{
    myServo.write(90);
  }
}
