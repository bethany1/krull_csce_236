
/*--------------------------------------------------------------------
Name:   Bethany Krull
Date:   02/08/2021
Course: CSCE 236 Embedded Systems (Spring 2021)
File:   rgbLED.ino
HW/Lab: Lab 1 

Purp:   cycle through a pattern of red/green/blue on the RGB LED 
        - pushbutton activated 

Doc:    I worked on my own.


Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/
const uint8_t redLED = 5;
const uint8_t greenLED = 6;
const uint8_t blueLED = 7;
const uint8_t button = 3;

void setup() {
  pinMode(redLED, OUTPUT);
  pinMode(greenLED, OUTPUT);
  pinMode(blueLED, OUTPUT);
  pinMode(button, INPUT);
}

void ledPattern(){
  //ONLY RED
  digitalWrite(redLED, HIGH);
  digitalWrite(greenLED, LOW);
  digitalWrite(blueLED, LOW);
  delay(1000);
  //GREEN AND RED
  digitalWrite(redLED, HIGH);
  digitalWrite(greenLED, HIGH);
  digitalWrite(blueLED, LOW);
  delay(1000);
  //ONLY GREEN
  digitalWrite(redLED, LOW);
  digitalWrite(greenLED, HIGH);
  digitalWrite(blueLED, LOW);
  delay(1000);
  //BLUE AND GREEN
  digitalWrite(redLED, LOW);
  digitalWrite(greenLED, HIGH);
  digitalWrite(blueLED, HIGH);
  delay(1000);
  //ONLY BLUE
  digitalWrite(redLED, LOW);
  digitalWrite(greenLED, LOW);
  digitalWrite(blueLED, HIGH);
  delay(1000);
  //RED AND BLUE
  digitalWrite(redLED, HIGH);
  digitalWrite(greenLED, LOW);
  digitalWrite(blueLED, HIGH);
  delay(1000);
  //ALL OFF
  digitalWrite(redLED, LOW);
  digitalWrite(greenLED, LOW);
  digitalWrite(blueLED, LOW);
  delay(2000);
  
}

void loop() {
  uint8_t buttonState = 0;
  buttonState = digitalRead(button);

  //IF BUTTON IS PUSHED - STARTS LED PATTERN
  if(buttonState == LOW){
    ledPattern();
    }
  //IF BUTTON IS NOT PUSHED - ALL LEDs OFF
  else {
    digitalWrite(redLED, LOW);
    digitalWrite(greenLED, LOW);
    digitalWrite(blueLED, LOW);
  }

}
