/*--------------------------------------------------------------------
Name:   Bethany Krull
Date:   02/09/21
Course: CSCE 236 Embedded Systems (Spring 2021)
File:   servo1Photo.ino
HW/Lab: Lab 1

Purp:   Use 1 photoresistors to move a servo motor to follow the
        brightest light.
        
Doc:    I worked on my own.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/
#include <Servo.h>

Servo myServo;
const uint8_t photoPin = A0;
const uint8_t servoPin = 11;
uint8_t servoPos = 0;

uint16_t center = 0;
uint16_t left = 0;
uint16_t right = 0;

//VARIABLES TO TUNE SYSTEM
uint8_t threshold = 10;
uint8_t angle = 10;
uint8_t pause = 100;

void setup() {
  myServo.attach(servoPin);
}

void loop() {
  //COLLECT LIGHT LEVELS LEFT/RIGHT OF CURRENT POSITION
  center = analogRead(photoPin);
  servoPos -= angle;
  myServo.write(servoPos);
  delay(pause);
  left = analogRead(photoPin);
  servoPos += (angle * 2);
  myServo.write(servoPos);
  delay(pause);
  right = analogRead(photoPin);
  servoPos -= angle;
  myServo.write(servoPos);
  delay(pause);


  //IF CENTER IS BRIGTEST
  if((center > left) && center > right){
    myServo.write(servoPos);
    delay(pause);
  }
  //IF LEFT IS BRIGHTEST
  else if((left > center) && (left - right) > threshold){
    servoPos -= angle;
    myServo.write(servoPos);
    delay(pause);
  }
  //IF RIGHT IS BRIGHTEST
  else if((right > center) && (right - left) > threshold){
    servoPos += angle;
    myServo.write(servoPos);
    delay(pause);
  }
}
