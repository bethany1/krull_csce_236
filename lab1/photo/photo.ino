/*--------------------------------------------------------------------
Name:   Bethany Krull
Date:   02/08/2021
Course: CSCE 236 Embedded Systems (Spring 2021)
File:   photo.ino
HW/Lab: Lab 1 

Purp:   

Doc:    


Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/

void setup() {
  Serial.begin(9600);
  Serial.println("Hello World");
}

void loop() {
  uint16_t val = analogRead(A0);
  Serial.println(val);
}
