/*--------------------------------------------------------------------
Name:   Bethany Krull
Date:   02/09/21
Course: CSCE 236 Embedded Systems (Spring 2021)
File:   servo2Photo.ino
HW/Lab: Lab 1

Purp:   Use 2 photoresistors to move a servo motor to follow the
        brightest light.
        
Doc:    I worked on my own.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/
#include <Servo.h>

Servo myServo;
const uint8_t photoPin1 = A0;
const uint8_t photoPin2 = A1;
const uint8_t servoPin = 11;

void setup() {
  myServo.attach(servoPin);
}

uint8_t average(uint8_t num1, uint8_t num2){
  uint8_t ave = (num1+num2)/2;
  return ave;
}

void loop() {
  //READ EACH PHOTORESISTOR
  //MAP VALUES FROM 0-1023 (PHOTO INPUT) TO 0-180 (SERVO ANGLES)
  uint16_t photoVal1 = analogRead(A0);
  photoVal1 = map(photoVal1, 0, 1023, 0, 180);
  uint16_t photoVal2 = analogRead(A1);
  photoVal2 = map(photoVal2, 0, 1023, 0, 180);

  //AVERAGE MAPPED ANGLE FROM BOTH PHOTORESITOR
  uint8_t averageAngle = average(photoVal1, photoVal2);
  myServo.write(averageAngle);

  //SET SERVO TO CALCULATED ANGLE
  if(photoVal1 > photoVal2){
    myServo.write(averageAngle);
  }
  else if(photoVal1 < photoVal2){
    myServo.write(-averageAngle);
  }
}
