/*--------------------------------------------------------------------
Name:   Bethany Krull
Date:   03/10/21
Course: CSCE 236 Embedded Systems (Spring 2021)
File:   servoSonic.cpp
HW/Lab: LAB 3

Purp:   Assemble the robot kit and get familiar with both the servo
        and the ultrasonic.

Doc:  I talked with the professor and Patrick about some board setup
      and servo setup.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
--------------------------------------------------------------------*/
#include <avr/io.h>
#include "sonicServo.h"

/*------------------------------------------------------------------
		sets up timers, I/O, serial port
-------------------------------------------------------------------*/
void setup() {
  //setup SERVO as output
  DDRD |= (1 << servo);
  
  //set timer2 PWM - phase correct  WGM: 0b101
  TCCR2A |= (1 << COM2B1) | (1 << COM2A1) | (1 << WGM20);   
  TCCR2B |= (1 << WGM22); 

  //set timer2 prescaler
  TCCR2B |= 7;             //1024 prescaler

  //set OCRs based on ultrasonic frequency
  OCR2A = 156;
  OCR2B = 8;

  //setup SONIC 
  //set sonicTrigger as output
  DDRB &= ~(1 << sonicEcho);
  DDRD |= (1 << sonicTrigger);

  //set up timer 1  -  detect falling edge
  TCCR1A = 0;   //normal mode
  TCCR1C = 0;
  //Set counter to zero, high byte first
  TCNT1H = 0;
  TCNT1L = 0;  
  //Make sure interrupts are disabled 
  TIMSK1 = 0;
  TIFR1 = 0;

  //set up LEDS as output
  DDRD |= (1 << leftLED);
  DDRD |= (1 << rightLED);

  Serial.begin(9600);
  Serial.println("begin....");
}


/*------------------------------------------------------------------------
	moves servo left/right/center depending on passed parameter	
-------------------------------------------------------------------------*/
void servoMove(uint8_t position){
  if(position == LEFT){
    OCR2B = 16;
    delay(1000);
  }
  else if(position == RIGHT){
    OCR2B = 3;
    delay(1000);
  }
  else if(position == CENTER){
    OCR2B = 8; 
    delay(1000);
  }
}


/*---------------------------------------------------------------------------
	calculates distance between robot and wall using ultra-sonic sensor
----------------------------------------------------------------------------*/
uint8_t sonicDist(){
  TCNT1 = 0;
  ICR1 = 0;
  //trigger 10 microsec
  PORTD |= (1 << sonicTrigger);
  delayMicroseconds(10); 
  PORTD &= ~(1 << sonicTrigger);

  while(ICR1 == 0){}
  uint16_t echoCount = (ICR1);           
  float distCm = echoCount * 0.010625;      //calculated distance in inches  
  //(time per cy (microsec) * speed of sound (cm/microsec) * 0.5)
  //(0.0625 * 0.034 * 0.5)

  delay(60);
  return distCm;
}


/*-------------------------------------------------------------------------------
	detects walls to left/right/center of robot and indicates walls via LEDs
-------------------------------------------------------------------------------*/
void sonicObjectDetect(){
  servoMove(LEFT);
  delay(1000);
  uint8_t distLeft = sonicDist();

  servoMove(CENTER);
  delay(1000);
  uint8_t distCenter = sonicDist();

  servoMove(RIGHT);
  delay(1000);
  uint8_t distRight = sonicDist();

  //sets appropriate LED depending on obstacle 
  //number adjusted to account for inaccurate dist math
  if(distLeft <= 10){        
    PORTD |= (1 << leftLED);
  }
  if(distRight <= 10){
    PORTD |= (1 << rightLED);
  }
  if(distCenter <= 10){
    PORTD |= (1 << leftLED) | (1 << rightLED);
  }
}









